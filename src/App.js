import React from 'react';
import ReactDOM from "react-dom";
import { Grid, Div, Row, Col, ThemeProvider, StyleReset } from 'atomize';

const theme = {
  colors: {
    primary: 'tomato',
    accent: 'yellow',
  },
};

function App() {

  return (

    <ThemeProvider theme={theme}>
      <StyleReset />

      <Div m={{ b: "4rem" }} bg="accent">
    <Row>
      {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(index => (
        <Col size={1} key={index}>
          hello world {index}
          <Div bg="primary" h="3rem" rounded="md" />
        </Col>
      ))}
    </Row>
  </Div>

    </ThemeProvider>
  );
}



export default App;
